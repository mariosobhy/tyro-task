class CreateStates < ActiveRecord::Migration[5.1]
  def change
    create_table :states do |t|
      t.string :device_name
      t.integer :os
      t.integer :memory
      t.integer :storage
      t.references :feedback, foreign_key: true

      t.timestamps
    end
  end
end

class CreateFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :feedbacks do |t|
      t.string :company_token
      t.integer :number
      t.integer :priority

      t.timestamps
    end
    add_index :feedbacks, :company_token
    add_index :feedbacks, :number
  end
end
